set args_string $::env(ARGS)
set args_raw [split $args_string " "]
set args [lmap value $args_raw {
    if {$value == {}} {
        continue
    } else {
        string cat $value
    }
}]

# Only some args are provided by the user. The top module and project root is provided by the
# swim configuration so 4 is deliberately not 2
if { [llength $args] != 4 } {
    puts "$args"
    puts "Expected 2 arguments: The test file and the test case name"
    puts "     Got ${args}"
    puts ""
} else {
    lassign $args projectRoot top testFile testCase 
    puts "Running with '${args_string}' ${args} (${top}, ${testFile}, ${testCase})"
    read_liberty /home/frans/.volare/sky130B/libs.ref/sky130_fd_sc_hd/lib/sky130_fd_sc_hd__ss_n40C_1v35.lib
    # read_verilog designs/${top}/runs/build/results/final/verilog/gl/${top}.v
    read_verilog runs/swim_run/final/nl/${top}.nl.v
    link_design ${top}
    read_sdc runs/swim_run/final/sdc/${top}.sdc
    read_spef runs/swim_run/final/spef/nom/${top}.nom.spef
    set vcd "${projectRoot}/build/${testFile}_${testCase}/${testFile}.vcd"
    read_power_activities -scope ${top} -vcd ${vcd}
    report_power
}
