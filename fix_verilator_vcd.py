import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("build_dir")
    parser.add_argument("test_file")
    parser.add_argument("test_case")
    args = parser.parse_args();

    vcd_file = args.build_dir + f"/{args.test_file}_{args.test_case}/{args.test_file}.vcd"

    print(f"Reading {vcd_file}")
    with open(vcd_file) as f:
        content = f.read()
        if "\n $scope module  $end" in content:
            print("Found verilator trash")
            content = content \
                .replace("\n $scope module  $end", "") \
                .replace("\n $upscope $end", "")
        else:
            return

    with open(vcd_file, "w") as f:
        print(f"Writing new VCD file to {vcd_file}")
        f.write(content)


if __name__ == "__main__":
    main()
