include Makefile

runInDocker:
	cd $(OPENLANE_DIR) && \
		$(ENV_COMMAND) sh -c "ARGS='$(TOP) $(ARGS)' ${COMMAND}"
