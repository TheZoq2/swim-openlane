import sys


def main():
    infile = sys.argv[1]
    outfile = sys.argv[2]
    top = sys.argv[3]

    print(f"Adding tracing to {infile} into {outfile} with {top}")

    tracing = """
    `ifdef COCOTB_SIM
    string __top_module;
    string __vcd_file;
    initial begin
        if ($value$plusargs("TOP_MODULE=%s", __top_module) && __top_module == "##TOP##" && $value$plusargs("VCD_FILENAME=%s", __vcd_file)) begin
            $dumpfile (__vcd_file);
            $dumpvars (0, ##TOP##);
        end
    end
    `endif
    """.replace("##TOP##", top)

    with open(infile) as f:
        lines = f.readlines()

    result = lines[:-1] + tracing.split("\n") + [lines[-1]]

    with open(outfile, 'w') as f:
        f.write("\n".join(result))

if __name__ == "__main__":
    main()
